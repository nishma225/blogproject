from django.db import models


class Blog(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='blogs')
    content = models.TextField()
    author = models.CharField(max_length=100)
    posted_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
