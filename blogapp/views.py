from django.views.generic import *
from .models import *
from .forms import *


class HomeView(TemplateView):
    template_name = 'home.html'


class AboutView(TemplateView):
    template_name = 'about.html'


class BlogListView(ListView):
    template_name = 'bloglist.html'
    queryset = Blog.objects.all().order_by('-id')
    context_object_name = 'bloglist'


class BlogDetailView(DetailView):
    template_name = 'blogdetail.html'
    queryset = Blog.objects.all()
    context_object_name = 'blogdetail'


class BlogCreateView(CreateView):
    template_name = 'blogcreate.html'
    form_class = BlogForm
    success_url = '/blog/list/'


class BlogUpdateView(UpdateView):
    template_name = 'blogcreate.html'
    queryset = Blog.objects.all()
    form_class = BlogForm
    success_url = '/blog/list/'


class BlogDeleteView(DeleteView):
    template_name = 'blogdelete.html'
    queryset = Blog.objects.all()
    success_url = '/blog/list/'
